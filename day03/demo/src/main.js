import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false
Vue.directive('ha', {
  inserted(el, binding, Vnode) {

    console.log(binding.modifiers);
    // console.log(Vnode);
    let str = 'on' + binding.arg;
    // console.log(str);
    // el[str] = binding.value
    // let obj = {
    //   name: '张三'
    // }
    // let str = 'name';
    // console.log(obj[str]);

    // console.log();
    el[str] = function (e) {
      // console.log(e.keyCode);
      if (e.keyCode == Object.keys(binding.modifiers)[0]) {
        binding.value(e);
      }
    }
  }
})
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
