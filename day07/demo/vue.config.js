const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    // host: "172.17.80.13", // 配置主机地址
    // port: 3001,
    // proxy: "http://172.17.80.13:3000",
    proxy: { // 配置多个代理
      "/abc": {
        target: "http://172.17.80.13:3000",
        ws: true, // 跨域地址是https协议！
        changeOrigin: true,
        pathRewrite: {
          "^/abc": ""   // 将 '/api' 替换成 ''  
        }
      },
      "/def": {
        target: "http://172.17.80.13:3000",
        ws: true, // 跨域地址是https协议！
        changeOrigin: true,
        pathRewrite: {
          "^/def": "/list"   // 将 '/api' 替换成 ''  
        }
      }
    }
  }
})
