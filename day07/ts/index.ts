let num:number = 10;
let str = "sdfghj";
// 在ts中如果声明变量的时候，不指定类型，根据初始值的类型，进行推断。如果没有初始值，（any）
// 
let bool;
// 在ts 类型一旦确定之后，不能赋值其他的类型给该变量
bool = "qwert";
bool = true;


console.log(bool);

console.log(str);

console.log(num)

function fn (val:number):void{
  console.log(val + 10)
}

