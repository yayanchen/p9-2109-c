import { createRouter, createWebHashHistory,RouteRecordRaw } from 'vue-router';
import home from '../views/home.vue';
// RouteRecordRaw 是一个接口
const routes:Array<RouteRecordRaw> = [
    {
        path: "/",
        name: "Home",
        component: home
    }, {
        path: "/login",
        name: "Login",
        component: () => import('../views/login.vue')
    }
]


const router = createRouter({
    history: createWebHashHistory(), // hash 模式
    routes,
})

export default router;