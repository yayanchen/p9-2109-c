import axios from 'axios';

const api = axios.create({
  baseURL: "http://8.141.145.150:3000",
  timeout: 5000
})

// 请求拦截
api.interceptors.request.use(config => {
  return config
},err => {
    return Promise.reject(err)
})


// 相应拦截
api.interceptors.response.use(res => {
    return res.data
  },err => {
      return Promise.reject(err)
  })

 export default api;