import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import bus from './utils/eventBus'
const app = createApp(App);
app.config.globalProperties.$bus = bus; // 全局挂在 bus 方法
app.use(store).use(router).mount('#app')
