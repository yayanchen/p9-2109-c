import { createStore } from 'vuex'
import persist from 'vuex-persistedstate'
export default createStore({
  strict: true, // 如果vuex 开启了严格模式，是不允许直接修改 state 值的
  state: {
    number: 10
  },
  getters: {
  },
  mutations: {
    add(state) {
      state.number++
    },
    kong() { // 为了实现 数据的存储

    }
  },
  actions: {
  },
  modules: {
  },
  plugins: [
    new persist({
      storage: window.localStorage,
    }),
  ],
})
