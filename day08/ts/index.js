"use strict";
// let numArr:number[] = [1,2,3,4]
// numArr.push(90);
// let numArr:Array<number> = [1,2,3,4]
// numArr.push('14')
// 元组 是允许追加选项 选项必须是 指定类型的子集
let tom = ["tom", 30];
// 最小的长度，必须是你声明，指定类型的长度
// tom[0] = "123";
// tom.push(false);
// console.log(tom);
// 类型推论 当你声明变量的时候 如果 没有明确的指定类型，会根据你赋值的类型进行推论
// 如果只是声明，没有指定类型，也没有赋值，就会默认推论成 any 类型。
// let num;
// num = "200";
// num = 20;
// 联合类型
let numStr;
numStr = "20";
numStr = 70;
// 在使用联合类型的时候，不确定是什么类型，只能使用公共方法。
console.log(numStr.toString());
function getLength(something) {
    // return something.toString().length;
    return something.length;
}
console.log(getLength('123456'));
