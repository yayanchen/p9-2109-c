import { createRouter, createWebHashHistory } from 'vue-router';
// 下面的 home 引入，不管用不用都会引入
import home from '../view/home.vue';
const routes = [
    {
        path: '/',
        name: "home",
        component: home
    }, {
        path: '/about',
        name: "About",
        component: () => import('../view/about.vue') // 只有用户访问 about 页面的时候，才会引入
    }
];

const router = createRouter({
    history: createWebHashHistory(),
    routes
})

export default router