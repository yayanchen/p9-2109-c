"use strict";
// function fn (val: string | number):number {
// //    return (val as string).length;
// return (<string>val).length;
// }
// // 类型断言有两种方式
// // 1.值 as 类型
// // <类型>值
// console.log(fn("你是个菜鸟"));
// console.log(fn(90));
let fn = (x, y) => {
    return x + y;
};
console.log(fn(1, 7));
// function fn:Ifn (x:number,y:number){
//     return x + y
// }
