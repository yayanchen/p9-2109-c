// function fn (val: string | number):number {
// //    return (val as string).length;
// return (<string>val).length;
// }
// // 类型断言有两种方式
// // 1.值 as 类型
// // <类型>值
// console.log(fn("你是个菜鸟"));
// console.log(fn(90));

// 当函数有返回值的时候，没有给函数约束返回值的类型 默认 any 
// function getNum(val: string | number):number | string{
//   return val
// }

// console.log(getNum(30));
// console.log(getNum("30"));


interface Ifn{
  (x:number,y:number): number
}


let fn:Ifn = (x,y) => {
   return x + y
}

console.log(fn(1,7));


// function fn:Ifn (x:number,y:number){
//     return x + y
// }