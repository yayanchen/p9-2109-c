import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
import persist from 'vuex-persistedstate'
import axios from 'axios';
import a from './a';
import b from './b';
export default new Vuex.Store({
  state: { // 存放数据的地方
    day: 600,
    goodList: []
  },
  getters: { // 相当于计算属性
    money(state) {
      return state.day * 40;
    }
  },
  mutations: { // 修改state里面的数据的方法
    add(state) {
      state.day++;
    },
    saveList(state, val) {
      state.goodList = val;
    }
  },
  actions: { // 执行异步操作的地方
    getGoods({ commit }) {
      // console.log(ctx);
      axios.get('data.json').then(res => {
        commit('saveList', res.data.data);
        // ctx.state.goodList = res.data.data;
        // console.log(this);
        // this.state.goodList = res.data.data;
      })
    }
  },
  modules: { // 模块化
    a, b
  },
  plugins: [
    new persist({
      storage: window.localStorage,
    }),
  ],
})
