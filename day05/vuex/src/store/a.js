export default {
    namespaced: true, // 开启命名空间，进行作用域隔离
    state: {
        num: 20
    },
    getters: {

    },
    mutations: {
        comNum(state) {
            state.num++
        }
    },
    actions: {

    }
}