"use strict";
// function addNum(x: number, y: number,z?: number):number {
//     return x+y+(z || 0)
// }
// // z 就是可选参数 (可选参数只能必选参数的末尾)
// console.log(addNum(10,20,40));
function getUser(name, age) {
    if (age) {
        return '我的名字' + name + '多大' + age;
    }
    else {
        return '我的名字' + name + '多大保密';
    }
}
