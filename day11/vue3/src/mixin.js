export default {
    data() {
        return {
            mixNum: 30
        }
    },
    created() {
        console.log('我是混入的声明周期');
    },
    methods: {
        getMsg() {
            return '我是一个菜鸟，我就想摆烂！'
        }
    }
}