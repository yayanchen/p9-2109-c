"use strict";
// interface IUser {
//     name:string,
//     age:number
// }
// interface 是用来定义接口的，接口可以描述对象，描述方法 描述数组
// 接口在描述对象的时候，必须和接口保持一致。
// let obj:IUser = {
//    name: "fghj",
//    age: 20,
// }
function request(params) {
    let ajax = new XMLHttpRequest();
    let url = params.url;
    if (params.method == "get" && params.data) {
        // "?id=10&age=18"
        for (let n in params.data) {
            url += url.includes("?") ? `&${n}=${params.data[n]}` : `?${n}=${params.data[n]}`;
        }
        ajax.open(params.method, url);
        ajax.send();
    }
    else {
        ajax.open(params.method, url);
        ajax.send(JSON.stringify(params.data));
    }
    ajax.onreadystatechange = function () {
        if (ajax.status == 200 && ajax.readyState == 4) {
            // console.log(ajax.response);  
            params.success(JSON.parse(ajax.response));
        }
    };
}
request({
    method: "post",
    url: "https://api.it120.cc/small4/user/m/login?deviceId=007&deviceName=monkey",
    data: { id: 10, age: 18 },
    success: function (val) {
        console.log(val);
    }
});
