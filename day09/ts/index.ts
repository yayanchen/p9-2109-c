// interface IUser {
//     name:string,
//     age:number
// }
// interface 是用来定义接口的，接口可以描述对象，描述方法 描述数组
// 接口在描述对象的时候，必须和接口保持一致。
// let obj:IUser = {
//    name: "fghj",
//    age: 20,
// }

// 接口可以描述可选属性 (在对象中写或者不写都没有问题)
// interface IUser {
//     name:string,
//     age:number,
//     gender?: string  // 定义可选属性
// }


// let tom:IUser = {
//     name: 'tom',
//     age: 18
// }

// 任意属性不确定属性名


// interface IUser {
//     readonly id: string // id为只读属性 (只读属性，不允许修改,声明对象的时候必须赋值，并不是第一次赋值)
//     name: string
//     age: number
//     gender?: string
//     [prop:string]: string | number | undefined  // 任意属性
// }

// let tom:IUser = {
//     id: "123456",
//     name: 'tom',
//     age: 18,
//     gender: "dfghjk",
//     six: "fdghjk",
//     num: 90
// }
// // tom.id = "789"
// tom.name = "李四"


// function fn (val:string){
//   console.log(val);

//   console.log(tom[val]);
// }

// fn('name')


// 实例化ajax核心方法
// let ajax = new XMLHttpRequest() 
// // 建立连接 open('请求方式','请求连接'，同步或者异步)
// // open 第一个参数，是请求方式 ，常见的有 get post, 第二个参数是请求地址，第三个参数是一个 boolean (是否是同步)
// ajax.open('post','https://api.it120.cc/small4/user/m/login?deviceId=007&deviceName=monkey')
// // 如果原生的 ajax get 请求，直接拼接到地址栏后面
// // 发送 post 请求的时候只能填写一个字符串
// // 发送请求
// ajax.send(JSON.stringify({mobile:'13500000000',pwd:'werty'}))
// // ajax状态判断
// // ready 准备的意思 state 状态的意思 change 改变
// ajax.onreadystatechange = function(){
//     // console.log(ajax.readyState);
//     // console.log(ajax.status);
//     // ajax.readyState 值 为 4 的时候，完成了请求处理
//     // response 相应的数据
//     if(ajax.status==200 && ajax.readyState==4){
//         console.log(ajax.response);
//     }

// }

interface Iparams {
    method: string, // 请求方式
    url: string, // 请求地址
    data?: any, // 请求参数
    success(val:any): void // 请求成功的回调函数
}



function request(params:Iparams){
    let ajax = new XMLHttpRequest()
    let url = params.url;
    if (params.method == "get" && params.data){
        // "?id=10&age=18"
        for (let n in params.data) {
            url += url.includes("?") ? `&${n}=${params.data[n]}` : `?${n}=${params.data[n]}`
        }
        ajax.open(params.method,url)
        ajax.send();
     } else {
        ajax.open(params.method,url)
        ajax.send(JSON.stringify(params.data))
     }
    ajax.onreadystatechange = function(){
        if(ajax.status==200 && ajax.readyState==4){
            // console.log(ajax.response);  
            params.success(JSON.parse(ajax.response));
        }
    }
}


request({
  method: "post",
  url: "https://api.it120.cc/small4/user/m/login?deviceId=007&deviceName=monkey",
  data:{id:10,age:18},
  success: function (val) {
      console.log(val);
  }
})