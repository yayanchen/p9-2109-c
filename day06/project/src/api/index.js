import axios from "axios";

const api = axios.create({
    baseURL: 'https://api.it120.cc/small4', // 公共的地址
    timeout: 5000 // 超时时间
})

api.interceptors.request.use(config => { // 请求拦截
    console.log('请求之前走的');
    return config
}, err => {
    return Promise.reject(err)
})



api.interceptors.response.use(res => { // 请求拦截
    console.log('相应回来走的');
    return Promise.resolve(res.data)
}, err => {
    return Promise.reject(err)
})

export default api