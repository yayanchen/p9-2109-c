class Tab {
    constructor(tabDom, contDom) { // tabDom 切换的节点 contDom 显示内容的节点
        this.dom = document.querySelectorAll(tabDom);
        // console.log(dom);
        this.content = document.querySelector(contDom);
        this.bindClick();
    }
    bindClick() { // 绑定点击事件
        for (let i = 0, item; (item = this.dom[i++]);) {
            item.onclick = this.domClick;
        }
    }
    domClick = (e) => { // 点击事件
        // console.log(this);
        this.dom.forEach((n) => {
            n.style.color = "#000";
        }); // 把所有的 都变成黑色
        e.target.style.color = "red";
        this.content.innerHTML = `欢迎进入${e.target.innerHTML}专区`;
    }
}



