// class People {
//     name: string
//     age: number
//     constructor(name:string,age:number){
//         this.name = name
//         this.age = age
//     }
//     sport():string{
//       return `${this.name}会运动`
//     }
// }
// let p = new People('佩洛西',82);
// console.log(p);


// class Animal {
//     name: string
//     constructor(name:string){
//         this.name = name
//     }
//     getName():string{
//         return this.name
//     }
// }

// class Dog extends Animal {
//     constructor(name:string){
//         super(name) // name 相当于我们说的实参
//     }
// }

// let d = new Dog('哈士奇')
// console.log(d.name); // 哈士奇
// console.log(d.getName());// 哈士奇



// class Animal {
//     private name: string // 公共属性
//     constructor(name:string){
//         this.name = name
//     }
//     getName():string{
//         // 父类内部获取name属性
//         return this.name
//     }
// }



// class Dog extends Animal {
//     constructor(name:string){
//         super(name)
//     }
//     run():string{
//         // 子类内部获取name属性
//         return this.name + '会运动'
//     }
// }
// // let dog = new Animal('哈士奇');
// // console.log(dog.name);
// // console.log(dog.getName());


// let obj = new Dog('哈士奇');
// console.log(obj.name);
// console.log(obj.run());

// public 修饰的属性，可以被本身，实例化对象，子类，访问。
// protected 修饰的属性，可以被本身,子类访问,不能被实例化出来的对象访问。
// private 修饰的属性，可以被本身访问。不能被子类和实例化出来的对象访问




// class Animal {
//     name:string
//     static prop: string = '1'
//     constructor(name:string){
//         this.name = name
//     }
//     getName():string{
//         return this.name
//     }
// }

// console.log(Animal.prop); // 1

// let a = new Animal('小狗')
// console.log(a);

// 静态属性，只能通过 类本身来访问
// console.log(a.prop); // 属性“prop”在类型“Animal”上不存在



// class Animal {
//     name:string
//     static prop: string = '1'
//     constructor(name:string){
//         this.name = name
//     }
//     getName():string{
//         return this.name
//     }
//     static sayHello():string{
//         return 'hello'
//     }
// }
// // Animal.prop = 'haha'

// console.log(Animal.prop); // 1

// console.log(Animal.sayHello()); // hello

// let a = new Animal('小狗')

// console.log(a.prop); // 属性“prop”在类型“Animal”上不存在
// console.log(a.sayHello()); // 性“sayHello”在类型“Animal”上不存在



// abstract class Animal { //abstract 修饰的这种类叫抽象类 （抽象类是不能被实例化的）
//     name: string
//     constructor(name:string){
//         this.name = name
//     }
//     getName():string{
//         return this.name
//     }
// }


// let obj = new Animal('布偶')



// abstract class Animal {
//     name: string
//     constructor(name:string){
//         this.name = name
//     }
//     abstract getName():string
// }
// // 狗类继承 动物类
// class Dog extends Animal {
//     constructor(name:string){
//         super(name)
//     }
//     getName():string{
//       return this.name
//     }
   
// }

// let obj = new Dog("哈士奇");
// console.log(obj.getName());

// 抽象的方法只能 存在于抽象类中。


// interface IAnimal {
//     name: string
//     eat():string
// }

// let obj:IAnimal = {
//   name:"张三",
//   eat(){
//     return this.name
//   }
// }

// interface Dog extends IAnimal {
//    age:number
// }

// class Animal implements Dog {
//     name: string;
//     age: number
//     constructor(name:string){
//         this.name = name
//         this.age = 20
//     }
//     eat(): string {
//         return ''
//     }
// }

// T 就代表泛型 
// function getVal<T>(name:T):T{
//     return name
// }

// getVal(90)

var arr:[string,number] = ['a',1]
	
function reverseArr<T,U>(arr:[T,U]):[U,T] {
    return [arr[1],arr[0]]
}

